import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlatformsListComponent } from './platforms-list/platforms-list.component';
import { EmptyGames } from './platforms-list/empty-games';
import { CreateGameComponent } from './create-game/create-game.component';
import { GamesComponent } from './platforms-list/games/games.component';
import { GameComponent } from './platforms-list/games/game/game.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'platform', component: PlatformsListComponent, children: [
      {path: '', component: EmptyGames},
      {path: ':id', component: GamesComponent}
    ]
  },
  {path: 'create', component: CreateGameComponent},
  {path: 'game', component: GameComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
