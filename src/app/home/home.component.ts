import { Component, OnInit } from '@angular/core';
import { GameModel } from '../shared/game.model';
import { GamesService } from '../shared/games.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  games!: GameModel[];

  constructor(private gamesService: GamesService) {
  }

  ngOnInit(): void {
    this.games = this.gamesService.getGames();
    this.gamesService.changeGames.subscribe((games: GameModel[]) => {
      this.games = games;
    });
  }

  showGame(game: GameModel) {
    this.gamesService.gamesPlatform = game;
  }
}
