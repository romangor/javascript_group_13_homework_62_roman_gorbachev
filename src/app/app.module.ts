import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateGameComponent } from './create-game/create-game.component';
import { GamesComponent } from './platforms-list/games/games.component';
import { PlatformsListComponent } from './platforms-list/platforms-list.component';
import { GameComponent } from './platforms-list/games/game/game.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FormsModule } from '@angular/forms';
import { GamesService } from './shared/games.service';
import { EmptyGames } from './platforms-list/empty-games';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateGameComponent,
    GamesComponent,
    PlatformsListComponent,
    GameComponent,
    ToolbarComponent,
    EmptyGames,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [GamesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
