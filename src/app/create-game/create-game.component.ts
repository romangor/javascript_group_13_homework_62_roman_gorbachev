import { Component, OnInit } from '@angular/core';
import { GameModel } from '../shared/game.model';
import { GamesService } from '../shared/games.service';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})
export class CreateGameComponent implements OnInit {
  name!: string;
  imageUrl!: string;
  platform!: string;
  description!: string;

  platforms!: string[];

  constructor(private gamesService: GamesService) {
  }

  ngOnInit(): void {
    this.platforms = this.gamesService.getPlatforms()
  }

  addGame() {
    this.gamesService.addGame(new GameModel(this.name, this.imageUrl, this.platform, this.description));
  }
}
