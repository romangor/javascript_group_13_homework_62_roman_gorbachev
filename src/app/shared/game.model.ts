export class GameModel {
  constructor(
    public name: string,
    public imageUrl: string,
    public platform: string,
    public description: string,
  ) {
  }
}
