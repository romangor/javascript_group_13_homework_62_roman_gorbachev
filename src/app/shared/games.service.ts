import { GameModel } from './game.model';
import { EventEmitter } from '@angular/core';

export class GamesService {
  changeGames = new EventEmitter<GameModel[]>();

  private games: GameModel[] = [
    new GameModel('game1', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCkSs7iKN0XrVBqYHZik8EyJzzttYs-W8xiw&usqp=CAU', 'PCS', 'Information about the game'),
    new GameModel('game2', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCkSs7iKN0XrVBqYHZik8EyJzzttYs-W8xiw&usqp=CAU', 'Mobile', 'Information about the game'),
    new GameModel('game3', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCkSs7iKN0XrVBqYHZik8EyJzzttYs-W8xiw&usqp=CAU', 'Mobile', 'Information about the game')
  ];
  gamesPlatform!: GameModel;

  private platforms: string[] = ['The Sony PlayStation', 'Microsoft’s Xbox', 'Nintendo’s Switch', 'PCS', 'Mobile'];

  getPlatforms() {
    return this.platforms.slice();
  }

  getGames() {
    return this.games.slice();
  }

  addGame(game: GameModel) {
    this.games.push(game);
  }

  getGamesByPlatform(platform: string) {
    return this.games.filter(game => platform === game.platform);
  }
}
