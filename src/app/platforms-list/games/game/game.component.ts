import { Component, OnInit } from '@angular/core';
import { GameModel } from '../../../shared/game.model';
import { GamesService } from '../../../shared/games.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  game!: GameModel;

  constructor(private gamesService: GamesService) {
  }

  ngOnInit(): void {
    this.game = this.gamesService.gamesPlatform;
  }
}

