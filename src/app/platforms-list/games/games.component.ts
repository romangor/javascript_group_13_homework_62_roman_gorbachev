import { Component, OnInit } from '@angular/core';
import { GameModel } from '../../shared/game.model';
import { GamesService } from '../../shared/games.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  games!: GameModel[];

  constructor(private gamesService: GamesService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const platform = params['id'];
      this.games = this.gamesService.getGamesByPlatform(platform);
    });
  }

  showGame(game: GameModel) {
    this.gamesService.gamesPlatform = game;
    this.router.navigate(['game']);
  }
}
