import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-games',
  template:
    `
      <div class="row">
        <div class="col-12 pt-5">
          <h2 class="text-center">Choose a platform</h2>
        </div>
      </div>`
})
export class EmptyGames {
}
