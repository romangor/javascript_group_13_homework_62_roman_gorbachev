import { Component, OnInit } from '@angular/core';
import { GamesService } from '../shared/games.service';

@Component({
  selector: 'app-platforms-list',
  templateUrl: './platforms-list.component.html',
  styleUrls: ['./platforms-list.component.css']
})
export class PlatformsListComponent implements OnInit {
  platforms!: string[];

  constructor(private gamesService: GamesService) {
  }

  ngOnInit(): void {
    this.platforms = this.gamesService.getPlatforms()
  }

}
